package com.greatlearning.week2assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    String accountNumber;
    String password;


    public static void main(String[] args) {
        Main main = new Main();
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("1234", "test@1234", 1000));
        customers.add(new Customer("5678", "test@5678", 2000));
        customers.add(new Customer("9012", "test@9012", 100));
        customers.add(new Customer("1357", "test@1357", 100000));
        customers.add(new Customer("2468", "test@2468", 5200));
        Scanner scanner = new Scanner(System.in);
        boolean exitStatus = true;

        while(exitStatus) {

            System.out.println("WELCOME TO INDIAN BANK");
            System.out.println("Enter your Account number");
            main.accountNumber = scanner.next();
            Customer customer = Customer.findCustomer(customers, main.accountNumber);

            if(customer == null){
                System.out.println("Customer does not exist");
                continue;
            }

            System.out.println("Enter your password: ");
            main.password = scanner.next();

            Login login = new Login();
            boolean loginStatus = login.loginVerify(main.accountNumber, main.password, customer);

            if (loginStatus){
                System.out.println("Login Successful");
                while (loginStatus){
                    System.out.println("1. Deposit");
                    System.out.println("2. Withdrawal");
                    System.out.println("3. Transfer");
                    System.out.println("0. Logout");
                    System.out.println("#. Exit Page");
                    System.out.println("Choose Operation");
                    String switchCase = scanner.next();
                    Transactions transactions = new Transactions();
                    switch(switchCase){
                        case "1": transactions.deposit(customer);
                                  break;
                        case "2": transactions.withdrawal(customer);
                                  break;
                        case "3": transactions.transfer(customers, customer);
                                  break;
                        case "0": loginStatus = false;
                                  break;
                        case "4": loginStatus = false;
                                  exitStatus = false;
                                  break;
                        default: loginStatus = false;
                                 break;
                    }
                }
            }else{
                System.out.println("Password incorrect. Please enter valid password.");
            }
        }
    }
}
