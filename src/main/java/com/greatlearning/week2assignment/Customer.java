package com.greatlearning.week2assignment;

import java.util.List;

public class Customer {
    private final String accountNumber;
    private final String password;
    private Integer balanceAmount;

    public Customer(String accountNumber, String password, Integer balanceAmount) {
        this.accountNumber = accountNumber;
        this.password = password;
        this.balanceAmount = balanceAmount;
    }


    public  String getAccountNumber() {
        return accountNumber;
    }

    public String getPassword() {
        return password;
    }

    public Integer getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Integer balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public static Customer findCustomer(List<Customer> customers, String accountNumber){
        return customers.stream().filter(customer1 -> customer1.getAccountNumber().equals(accountNumber)).findAny().orElse(null);
    }
}
