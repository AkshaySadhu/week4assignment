package com.greatlearning.week2assignment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Transactions {

    private String fileName = "C:/Users/urani/IdeaProjects/GreatlerningWeek4/src/main/resources/transactions.txt";

    public void deposit(Customer customer) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Deposit Amount");
        Integer depositAmount = scanner.nextInt();
        Integer newBalance = customer.getBalanceAmount() + depositAmount;
        customer.setBalanceAmount(newBalance);
        try {
            File file = new File(fileName);
            if (file.createNewFile()) {
                System.out.println("New file created");
            } else {
                System.out.println("File already exists in path");
            }

            FileWriter fileWriter = new FileWriter(file.getAbsolutePath(), true);
            String writeToFile = "Deposited " + depositAmount + " into Account Number: " + customer.getAccountNumber() + ". New Balance is: " + newBalance;
            fileWriter.write(writeToFile);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void withdrawal(Customer customer) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Withdrawal Amount");
        Integer withdrawalAmount = scanner.nextInt();
        Integer newBalance = customer.getBalanceAmount() - withdrawalAmount;
        customer.setBalanceAmount(newBalance);
        try {
            File file = new File(fileName);
            if (file.createNewFile()) {
                System.out.println("New file created");
            } else {
                System.out.println("File already exists in path");
            }

            FileWriter fileWriter = new FileWriter(file.getAbsolutePath(), true);
            String writeToFile = "Withdrew " + withdrawalAmount + " from Account Number: " + customer.getAccountNumber() + ". New Balance is: " + newBalance;
            fileWriter.write(writeToFile);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void transfer(List<Customer> customerList, Customer customer) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the account number to which you want to transfer money: ");
        String accountNumber = scanner.next();
        Customer receivingCustomer = Customer.findCustomer(customerList, accountNumber);

        if (customer == null) {
            System.out.println("Customer does not exist");
            return;
        }
        System.out.println("Enter amount to be transferred");
        Integer transferAmount = scanner.nextInt();

        String otp = otpGenerator();
        System.out.println("Your OTP is: " + otp);
        System.out.println("Enter OTP: ");
        String otpVerify = scanner.next();
        if(verifyOTP(otp, otpVerify)) {

            Integer newBalance = customer.getBalanceAmount() - transferAmount;
            customer.setBalanceAmount(newBalance);

            newBalance = receivingCustomer.getBalanceAmount() + transferAmount;
            receivingCustomer.setBalanceAmount(newBalance);

            try {
                File file = new File(fileName);
                if (file.createNewFile()) {
                    System.out.println("New file created");
                } else {
                    System.out.println("File already exists in path");
                }

                FileWriter fileWriter = new FileWriter(file.getAbsolutePath(), true);
                String writeToFile = "Transferred " + transferAmount + " from Account Number: " + customer.getAccountNumber() + "to Account Number" + receivingCustomer.getAccountNumber();
                fileWriter.write(writeToFile);
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            System.out.println("Incorrect OTP. Transaction cancelled");
        }

    }

    private String otpGenerator() {
        int randomPin = (int) (Math.random() * 9000) + 1000;
        String otp = String.valueOf(randomPin);
        return otp;

    }

    private boolean verifyOTP(String otp, String otpVerify){
        if(otp.equals(otpVerify)){
            return true;
        }
        return false;
    }
}
